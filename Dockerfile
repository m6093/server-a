FROM node:16.9.0

WORKDIR /home/usr/app

COPY . .

RUN npm ci && npm run build

EXPOSE 3000

CMD ["npm", "run", "start"]