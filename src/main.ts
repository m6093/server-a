import { NestFactory } from "@nestjs/core";
import {
  FastifyAdapter,
  NestFastifyApplication,
} from "@nestjs/platform-fastify";
import { PetsModule } from "./pets.module";

async function bootstrap(): Promise<void> {

  const app = await NestFactory.create<NestFastifyApplication>(
    PetsModule,
    new FastifyAdapter()
  );

  await app.listen(3000, "0.0.0.0");
}

void bootstrap();
