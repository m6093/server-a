import request from "supertest";

describe("pets (e2e)", () => {
  const req = request("http://localhost:3000");

  const cats = [{
    name: "meow",
    age: 1,
    breed: "cat",
  }];

  const dogs = [{
    name: "woof",
    age: 1,
    breed: "dog",
  }];

  test("get some cats", async () => {
    await req
      .get("/cats")
      .expect((res) => {
        expect(res.body).toMatchObject(cats);
      })
      .expect(200);
  });

  test("get some dogs", async () => {
    await req
      .get("/dogs")
      .expect((res) => {
        expect(res.body).toMatchObject(dogs);
      })
      .expect(200);
  });
});
